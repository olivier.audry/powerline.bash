#!/bin/bash -eu

i=1
for rvb in {1..8}{1..8}{01..16} ; do
	r=${rvb:0:1}
	v=${rvb:1:1}
	b=${rvb:2:2}
	r=$(( ${r/#0} * 32 - 1))
	v=$(( ${v/#0} * 32 - 1))
	b=$(( ${b/#0} * 16 - 1))

	couleur="48:2:$r:$v:$b"
	printf -v html "%02x%02x%02x" $r $v $b
        echo -en "\\e[${couleur}m $html "

	if [ "$(( i % 16 ))" = 0 ] ; then
		echo -e "\\e[0m"
	fi
	i=$((i+1))
done
