#!/bin/bash
# À sourcer dans un terminal
#
# Usage: tests/sourcer.sh POWERLINE_DIRECTION=rtl

unset ${!POWERLINE_*}
declare "$@"
. powerline.bash
