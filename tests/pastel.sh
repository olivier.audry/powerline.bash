#!/bin/bash
#
set -eu

. powerline.bash

for root in 0 1 ; do
	echo "ROOT=$root"
	for h in {0..47} ; do
		__powerline_hostname_color24 "$root" "$h"
		hue="${__powerline_retval[0]}"
		sat="${__powerline_retval[1]}"

		__powerline_hsl2rgb "${__powerline_retval[@]}"
		r="${__powerline_retval[0]}"
		v="${__powerline_retval[1]}"
		b="${__powerline_retval[2]}"

		printf -v texte "${hue/0.},${sat/0.} %02x%02x%02x" "$r" "$v" "$b"
		echo -en "\\e[48;2;$r;$v;$b;38;5;253m $texte \\e[0m"

		if [ $sat = "0.8" ] || [ $sat = "0.4" ] ; then
		echo
		fi
	done
done
